# Firefox configuration

![Browser screenshot](Screenshot.png)

This configuration aims to make Firefox looks beatiful while using sideberry to handle tabs in Firefox making a great user experience.

It contains the configuration of sideberry and the `userChrome.css` to remove the tabs of Firefox.

## Requirements

You need to have Firefox and sideberry installed.

## Installation

1. Go to `about:config` (enter it in the url field of Firefox)
1. Search for the parameter: `toolkit.legacyUserProfileCustomizations.stylesheets` and set it to true
1. Go to `about:support`
1. Search for the line "Profile Folder"
1. Open it in your file manager
1. If it doesn't exist, create a `chrome` folder
1. Copy the `userChrome.css` file into your new and desperately empty `chrome` folder
1. Right click on the sideberry extension button and press "Open settings"
1. Go to the "Help" section and "Import addon data"
1. Select the file: `sideberry-config.json`
1. Enjoy!!

## Bonus - The list of my addons

Some anti-tracking/anti-ads addons:

- [uBlock Origin](https://addons.mozilla.org/en-GB/firefox/addon/ublock-origin)
- [Privacy Badger](https://addons.mozilla.org/en-GB/firefox/addon/privacy-badger17)
- [Decentraleyes](https://addons.mozilla.org/en-GB/firefox/addon/decentraleyes/)
- [Don't track me Google](https://addons.mozilla.org/en-GB/firefox/addon/dont-track-me-google1)
- [ClearURLs](https://addons.mozilla.org/en-GB/firefox/addon/clearurls/)
- [CanvasBlocker](https://addons.mozilla.org/en-GB/firefox/addon/canvasblocker/)
- [User-Agent Switcher and Manager](https://addons.mozilla.org/en-GB/firefox/addon/user-agent-string-switcher/)

The others:

- [Bitwarden - Free Password Manager](https://addons.mozilla.org/en-GB/firefox/addon/bitwarden-password-manager/): My password manager of choice
- [Firefox Multi-Account Containers](https://addons.mozilla.org/en-GB/firefox/addon/multi-account-containers/): More options for firefox containers (used to isolate applications or connect with multiple accounts on the same website)
- [Grammar and Spell Checker - LanguageTool](https://addons.mozilla.org/en-GB/firefox/addon/languagetool/): A very good and open source spell checker that can be self-hosted
- [LibRedirect](https://addons.mozilla.org/en-GB/firefox/addon/libredirect/): Redirect some website to libre frontends (I use it to redirect all youtube links to my piped instance)
- [New Tab Override](https://addons.mozilla.org/en-GB/firefox/addon/new-tab-override/): Self-explanatory, I use it to open my homelab's' dashboard in each new tab
- [Omnivore](https://addons.mozilla.org/en-GB/firefox/addon/omnivore/): A plugin to send webpages to my read-it later app
- [Sidebery](https://addons.mozilla.org/en-GB/firefox/addon/sidebery/): A better way to handle tabs
- [SimpleLogin: Open-source Email Protection](https://addons.mozilla.org/en-GB/firefox/addon/simplelogin/): A service to generate one-time emails and send them to your personal email account
- [Video Speed Controller](https://addons.mozilla.org/en-GB/firefox/addon/videospeed/): Custom speed for videos, it works almost everywhere and allow you to accelerate videos by insane amount (I use it to watch live replay in x3-4 which is not allowed natively by Youtube or Piped)
- [Vimium](https://addons.mozilla.org/en-GB/firefox/addon/vimium-ff/): Browse the web with VIM-inspired keyboard shortcuts

On linux:

- [GNOME Shell integration](https://addons.mozilla.org/en-GB/firefox/addon/gnome-shell-integration/): An extension to easily install Gnome shell extensions through [the gnome extensions website](https://extensions.gnome.org/)

## Inspiration

- [Medium article of Xilin Sun](https://medium.com/@Aenon/firefox-hide-native-tabs-and-titlebar-f0b00bdbb88b)
- [A blog article of jahed.dev](https://jahed.dev/2021/10/08/compacting-firefox-layout-with-sidebery-and-user-chrome/)
